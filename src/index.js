'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const PORT = 8070;
const HOST = '0.0.0.0';
app.listen(PORT, HOST, ()=> {
    console.log('listening to port ' + PORT + ' host ' + HOST);
})

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const report_quotation = require('./routes/report_quotation');
app.post('/report_quotation', report_quotation);
