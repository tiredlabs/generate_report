// Generate Report Quotationn

const carbone = require('carbone');
const fs = require('fs');
const path = require('path');

module.exports = async (req, res) => {
    const data = {
        json: req.body,
    }
    
    var exportPath = './data/export';
    var exportName = data.json.filename + '.pdf';
    var exportFullPath = path.join(exportPath, exportName);

    var templatePath = './data/templates';
    var templateName = 'template_quotation.odt';
    var templateFullPath = path.join(templatePath, templateName);

    var dataPath = '../../data/json';
    var dataFilename = 'report_quotation.json';
    var dataFullPath = path.join(dataPath, dataFilename);

    let options = {
        convertTo: 'pdf',
    }

    carbone.render(templateFullPath, data.json, options, (err, result) => {
        if (err) {
            console.log("Error Carbone Render :" + err);
            // res.send(err);
            res.sendStatus(500);

        } else {
            // write the result
            console.log('File processed : ' + dataFilename);
            console.log('File exported path : ' + exportFullPath);
            // console.log('File json : %j', dataJson);

            fs.writeFile(exportFullPath, result, function () {
                console.log('File write success : ' + exportFullPath);

                res.download(exportFullPath, exportName, function (err) {
                    if(err){
                        console.log("Error Download File : " + err);
                    }
                    else{
                        console.log('File download success : ' + exportFullPath);
                    }

                });

            });

        }

    });
}