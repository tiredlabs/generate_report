Terminal or CLI

Install carbone
$ npm install carbone --save

To Build
$ docker build -t report_quotation:version1.0 .

To Run
$ docker run -v $(pwd)/data/export:/app/data/export -dp 8070:8070 report_quotation:version1.0


Generate the report data in json file named "report_quotation.json" then save it to ./data/json

Postman

choose POST and paste URL "localhost:8070/report_quotation" then press "Send and Download" beside "Send" button.