const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'product received'
    });
});

router.get('/:type', (req, res, next) => {
    const reportType = req.params.type;
    if (reportType === "quotation") {
        res.status(200).json({
            message: 'Product With Type',
            reportType: reportType
        })
    }
    else {
        res.status(200).json({
            message: 'Nah',
        })
    }
});

router.patch('/', (req, res, next) => {
    res.status(200).json({
        message: 'Updated'
    });
});

module.exports = router;