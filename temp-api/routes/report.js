const http = require('http');
const fs = require('fs');
const stream = require('stream');
const carbone = require('carbone');
const express = require('express');
const app = express();
const router = express.Router();
const path = require('path');

router.post('/:reportType', (req, res, next) => {
    const report = {
        type: req.params.reportType,
        data: req.body,
    };

    if (report.type === undefined) {
        console.log('Report Type cannot be undefined.');
        throw new Error('Report Type cannot be undefined.');
    }

    let options = {
        convertTo: 'pdf'
    }

    var exportPath = 'exported_report';
    var exportName = 'report_quotation.pdf';

    var templatePath = 'templates';
    var templateName;
    switch (report.type) {
        case 'quotation':
            templateName = 'template_quotation.odt';
            break;

        default:
            throw new Error('Report type not found.');
    }

    // Generate a report using the sample template provided by carbone module
    // This LibreOffice template contains "Hello {d.firstname} {d.lastname} !"
    // Of course, you can create your own templates!
    carbone.render(path.join(templatePath, templateName), report.data, options, function (carboneError, carboneResult) {
        if (carboneError) {
            return console.log('Carbone Error : ' + carboneError);
        }

        if (!fs.existsSync(exportPath)) {
            fs.mkdirSync(exportPath)

        }

        // write the result
        
        var fullFilename = path.join(exportPath, exportName);
        fs.writeFile(fullFilename, carboneResult, function () {
            console.log('File exported : ' + fullFilename);
            res.download(fullFilename, exportName, function (fsError) {
                console.log(fsError);
            });
        });

        // app.get(fullFilename, function (request, response) {
        //     //...
        //     // var fileContents = Buffer.from(carboneResult, "base64");
        //     var readStream = new stream.PassThrough();

        //     readStream.end(carboneResult);

        //     response.set('Content-disposition', 'attachment; filename=' + fileName);
        //     response.set('Content-Type', 'text/plain');

        //     readStream.pipe(response);
        // });


        // app.get('/', function(req, res) {
        //     res.contentType('text/plain');
        //     res.send('This is the content', { 'Content-Disposition': 'attachment; filename=name.txt' }); 
        //   });

        // res.status(201).json({
        //     message: 'Report created :' + report.type
        // });

        // app.get('/download', function(request, response){
        //     //...
        //     var fileContents = Buffer.from(fileData, "base64");
        //     var savedFilePath = '/temp/' + fileName; // in some convenient temporary file folder
        //     fs.writeFile(savedFilePath, fileContents, function() {
        //       response.status(200).download(savedFilePath, fileName);
        //     });
        //   });

    });


});

module.exports = router;