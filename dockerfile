FROM ideolys/carbone-env-docker
WORKDIR /app
COPY . /app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8070
ENTRYPOINT ["node", "src/index.js"]